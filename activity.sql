-- Insert users
INSERT INTO users(email, password, datetime_create) VALUES(
	"johnsmith@gmail.com",
	"passwordA",
	"2021-01-01 1:00:00" 
);

INSERT INTO users(email, password, datetime_create) VALUES(
	"juandelacruz@gmail.com",
	"passwordB",
	"2021-01-01 2:00:00" 
);

INSERT INTO users(email, password, datetime_create) VALUES(
	"janesmith@gmail.com",
	"passwordC",
	"2021-01-01 3:00:00" 
);

INSERT INTO users(email, password, datetime_create) VALUES(
	"mariadelacruz@gmail.com",
	"passwordD",
	"2021-01-01 4:00:00" 
);

INSERT INTO users(email, password, datetime_create) VALUES(
	"johndoe@gmail.com",
	"passwordE",
	"2021-01-01 5:00:00" 
);

-- Insert posts to the blog_db;

INSERT INTO posts(user_id, title, content, datetime_posted) VALUES(
	2,
	"First Code",
	"Hello World!",
	"2021-01-02 1:00:00"
);

INSERT INTO posts(user_id, title, content, datetime_posted) VALUES(
	2,
	"Second Code",
	"Hello Earth!",
	"2021-01-02 2:00:00"
);

INSERT INTO posts(user_id, title, content, datetime_posted) VALUES(
	3,
	"Third Code",
	"Welcome to Mars!",
	"2021-01-02 3:00:00"
);

INSERT INTO posts(user_id, title, content, datetime_posted) VALUES(
	5,
	"Fourth Code",
	"Bye bye solar system!",
	"2021-01-02 4:00:00"
);

-- Get all the post with an author id of johnsmith
SELECT * FROM posts WHERE user_id = 2;

-- Get all the user's email and datetime of creation
SELECT email, datetime_create FROM users;

-- Update a post's content to "Hello to the people of the Earth!" where its initial content is "Hello Earth" by using the record's ID.

UPDATE posts SET content = "Hello to the people of the Earth!" WHERE id = 2;

-- Delete the user with an email of "johndoe@gmail.com"

DELETE FROM users WHERE email = "johndoe@gmail.com";